import { etfAdapter } from '../adapters/etfAdapter'

export const fetchAllETFs = () => {
  const response = etfAdapter.fetchAllETFs()

  return {
    type: 'FETCH_ALL_ETFS',
    payload: response
  }
}
