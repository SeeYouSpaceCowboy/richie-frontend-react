import { groupAdapter } from '../adapters/groupAdapter'

export const fetchGroup = id => {
  const response = groupAdapter.fetchGroup(id)

  return {
    type: 'FETCH_GROUP',
    payload: response
  }
}

export const createGroup = (group) => {
  const response = groupAdapter.createGroup(group)

  return {
    type: 'CREATE_GROUP',
    payload: response
  }
}

export const groupRequestResponse = (id, choice) => {
  const response = groupAdapter.groupRequestResponse(id, choice)

  return {
    type: 'GROUP_REQUEST_RESPONSE',
    payload: response
  }
}

export const suggestEtf = (id, ticker, amount) => {
  const response = groupAdapter.suggestEtf(id, ticker, amount)

  return {
    type: 'SUGGEST_ETF',
    payload: response
  }
}
