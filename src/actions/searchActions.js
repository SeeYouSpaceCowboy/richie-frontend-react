import { searchAdapter } from '../adapters/searchAdapter'

export const fetchSearchResults = (input) => {
  const response = searchAdapter.fetchSearchResults(input)

  return {
    type: 'FETCH_SEARCH_RESULTS',
    payload: response
  }
}
