import { userAdapter } from '../adapters/userAdapter'

export const postUserFacebookResponse = (facebook_user) => {
  const response = userAdapter.postUserFacebookResponse(facebook_user)

  return {
    type: 'POST_USER_FACEBOOK_RESPONSE',
    payload: response
  }
}

export const fetchCurrentUser = () => {
  const response = userAdapter.fetchCurrentUser()

  return {
    type: 'FETCH_CURRENT_USER',
    payload: response
  }
}

export const fetchUser = (userID) => {
  const response = userAdapter.fetchUser(userID)

  return {
    type: 'FETCH_USER',
    payload: response
  }
}

export const logout = () => {
  const response = userAdapter.logout()

  return {
    type: 'LOGOUT',
    payload: response
  }
}
