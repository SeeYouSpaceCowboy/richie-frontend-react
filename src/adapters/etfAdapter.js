import axios from 'axios'
import { fbHeaderManager } from '../managers/fbHeaderManager'

const dev_url = 'https://richie-isuruv.c9users.io/v1/'

axios.defaults.baseURL = dev_url

export const etfAdapter = {
  fetchAllETFs: () => {
    return axios.get(`/etfs`, fbHeaderManager.getHeaders())
      .then(response => response.data)
      .catch(error => error)
  }
}
