import axios from 'axios'
import { fbHeaderManager } from '../managers/fbHeaderManager'

const dev_url = 'https://richie-isuruv.c9users.io/v1/'

axios.defaults.baseURL = dev_url

export const groupAdapter = {
  fetchGroup: id => {
    return axios.get(`/groups/${ id }`, fbHeaderManager.getHeaders())
      .then(response => response.data)
      .catch(error => error)
  },

  createGroup: group => {
    return axios.post('/groups', group, fbHeaderManager.getHeaders())
      .then(response => response.data)
      .catch(error => error)
  },

  groupRequestResponse: (id, choice, minimum) => {
    return axios.patch(`/group_requests/${ id }?approve=${ choice }?minimum=${ minimum }`, null, fbHeaderManager.getHeaders())
      .then(response => response.data)
      .catch(error => error)
  },

  suggestEtf: suggestion => {
    return axios.post('/suggestions', suggestion, fbHeaderManager.getHeaders())
      .then(response => response.data)
      .catch(error => error)
  }
}
