import axios from 'axios'
import { fbHeaderManager } from '../managers/fbHeaderManager'

const dev_url = 'https://richie-isuruv.c9users.io/v1/'

axios.defaults.baseURL = dev_url

export const searchAdapter = {
  fetchSearchResults: (input) => {
    return axios.get(`/users/search/${ input }`, fbHeaderManager.getHeaders())
      .then((response) => response.data)
      .catch((error) => error)
  }
}
