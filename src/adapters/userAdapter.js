import axios from 'axios'
import { browserHistory } from 'react-router'
import { fbHeaderManager } from '../managers/fbHeaderManager'

const dev_url = 'https://richie-isuruv.c9users.io/v1/'
const dev_auth_url = 'https://richie-isuruv.c9users.io/auth/'

axios.defaults.baseURL = dev_url

const useAuthURL = (callback) => {
  axios.defaults.baseURL = dev_auth_url
  let callbackResponse = callback()
  axios.defaults.baseURL = dev_url

  return callbackResponse
}

export const userAdapter = {
  postUserFacebookResponse: (facebook_user) => {
    return axios.post('user', facebook_user)
      .then((response) => (response.data.status === 200) ? fetchToken(facebook_user) : false)
      .catch((error) => error)
  },

  fetchCurrentUser: () => {
    return axios.get('user', fbHeaderManager.getHeaders())
      .then((response) => response.data)
      .catch((error) => error)
  },

  fetchUser: (userID) => {
    return axios.get(`users/${ userID }`, fbHeaderManager.getHeaders())
      .then((response) => response.data)
      .catch((error) => error)
  },

  logout: () => {
    let data = useAuthURL(() => {
      axios.delete('sign_out', fbHeaderManager.getHeaders())
        .then((response) => response)
        .catch((error) => error)
    })

    browserHistory.push('/login')
    return data
  }
}

const fetchToken = ({ user }) => {
  const devise = {
    email: user.email,
    password: user.fd_id,
    password_confirmation: user.fd_id,
    confirm_success_url: ""
  }

  let data = useAuthURL(() => {
    return axios.post('sign_in', devise, { headers: { CONTENT_TYPE: 'application/json' } })
      .then((response) => {
        fbHeaderManager.setHeaders(response.headers)
        return response.data.data
      }).catch((error) => error)
  })

  return data
}
