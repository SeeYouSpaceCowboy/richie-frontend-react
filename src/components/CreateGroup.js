import React, { Component } from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { createGroup } from '../actions/groupActions'

class CreateGroup extends Component {
  constructor() {
    super()
    this.state = {
      displayFriendsToggle: true,
      group: {
        name: '',
        description: '',
        minimum_amount: 0,
        public: false,
        access_tokens: [],
      }
    }

    this.handleChange           = this.handleChange.bind(this)
    this.handleSelectFriends    = this.handleSelectFriends.bind(this)
    this.renderFriendsList      = this.renderFriendsList.bind(this)
    this.addFriendToGroup       = this.addFriendToGroup.bind(this)
    this.handleGroupSubmit      = this.handleGroupSubmit.bind(this)
  }

  handleChange(event){
    const field = event.target.name
    const group = this.state.group
    group[field] = event.target.value
    return this.setState({ group })
  }

  handleSelectFriends() {
    let displayFriendsToggle = this.state.displayFriendsToggle
    displayFriendsToggle ? this.setState({ displayFriendsToggle: false }) : this.setState({ displayFriendsToggle: true })

    let $friendsDropdown = document.getElementById('friends-dropdown')
    displayFriendsToggle ? $friendsDropdown.style.display = 'block' : $friendsDropdown.style.display = 'none'
  }

  renderFriendsList(friends) {
    if(friends.length === 0) return <p>You have no friends D:</p>

    return (
      <ul id='friends-dropdown' className='create-group-friends-dropdown'>
        {
          friends.map((friend, i) => {
            return (
              <li key={ i } values={ friend.name }>
                <input onClick={ this.addFriendToGroup } type="checkbox" name={ friend.access_token }/> { friend.name }
              </li>
            )
          })
        }
      </ul>
    )
  }

  addFriendToGroup(event) {
    let token = event.target.name
    let group = this.state.group
    let index = group.access_tokens.indexOf(token)

    if(index > -1){
      group.access_tokens.splice(index, 1)
      this.setState({ group: group })
    } else {
      group.access_tokens.push(token)
      this.setState({ group: group })
    }
  }

  handleGroupSubmit() {
    this.props.createGroup(this.state.group)
    browserHistory.push('/groups')
  }

  render() {
    const currentUser = this.props.userState.currentUser
    if(Object.keys(currentUser).length === 0 && currentUser.constructor === Object) return null

    return (
      <div>
        <label>Group Name</label>
        <br/>
        <input className='fill' name='name' onChange={ this.handleChange }/>
        <br/>
        <br/>

        <label>Description</label>
        <br/>
        <input className='fill' name='description' onChange={ this.handleChange }/>
        <br/>
        <br/>

        <label>Minimum Amount</label>
        <br/>
        <input className='fill' name='minimum_amount' onChange={ this.handleChange }/>
        <br/>
        <br/>

        <label className="switch">
          Public <input type="checkbox" name='public'/>
        </label>
        <br/>
        <br/>

        <button onClick={ this.handleSelectFriends }>Select Friends</button>
        {
          this.renderFriendsList(currentUser.fb_friends)
        }
        <br/>
        <br/>

        <button onClick={ this.handleGroupSubmit }>Submit</button>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    userState: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    createGroup: group => {
      let action = createGroup(group)
      dispatch(action)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateGroup)
