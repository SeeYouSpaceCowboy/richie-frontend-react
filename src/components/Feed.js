import React, { Component } from 'react'
import FeedItem from './FeedItem'

export default class Feed extends Component {  
  deleteMe() {
      let randomArray = [...Array(25).keys()]

      let people = [
        { img: 'person-1.png', name: 'Ashley Claudia'},
        { img: 'person-2.png', name: 'Velia Grace' },
        { img: 'person-3.png', name: 'Emma Asa' },
        { img: 'person-4.png', name: 'Jason Clay' },
        { img: 'person-5.png', name: 'Ben Grayson' },
        { img: 'person-6.png', name: 'John Benson' }
      ]

      let mixes = [
        'Technology',
        'Agriculture',
        'Clean Energy',
        'Fossil Fuels & Oils',
        'Science',
        'Mature Stocks',
        'Advanced Economics',
        'Food',
        'Small Businesses',
        'Biology',
        'Real Estate',
        'International Trade'
      ]

      let decision = [
        'invested in',
        'sold'
      ]

      for(let x = 0; x < randomArray.length; x++) {
        let peopleRandomIndex = Math.floor(Math.random() * people.length)
        let mixesRandomIndex = Math.floor(Math.random() * mixes.length)
        let decisionRandomIndex = Math.floor(Math.random() * decision.length)
        let randomHour = Math.floor(Math.random() * 5) + 1

        randomArray[x] = {
          img: people[peopleRandomIndex]['img'],
          name: people[peopleRandomIndex]['name'],
          investment: mixes[mixesRandomIndex],
          choice: decision[decisionRandomIndex],
          time: randomHour
        }
      }

      return randomArray
  }

  render() {
    return (
      <div className='feed'>
        <div>
          {
            this.deleteMe().map((person, i) => {
              return <FeedItem person={ person } key={ i } />
            })
          }
        </div>
      </div>
    )
  }
}
