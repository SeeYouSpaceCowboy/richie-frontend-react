import React, { Component } from 'react'

export default class FeedItem extends Component {
  constructor() {
    super()

    this.handleFeedItemClick = this.handleFeedItemClick.bind(this)
  }

  handleFeedItemClick() {

  }

  render() {
    return (
      <div className='feed-item' onClick={ this.handleFeedItemClick }>
        <div className='row'>
          <div className='col-2'>
            <img className='left icon' alt='feed item' src={ `./images/delete-me/${ this.props.person['img'] }` } />
          </div>

          <div className='col-6'>
            <p>
              <span className='heading-1'>{ this.props.person['name'] }</span> has { this.props.person['choice'] }
              <span className='heading-1'>{ ` ${ this.props.person['investment'] }` }</span>
            </p>
            <p className='roboto'>{ this.props.person['time'] }h ago</p>
          </div>

          <div className='col-2'>
              <button className='fill'>INVEST</button>
          </div>

          <div className='col-2'>
              <button className='fill'>SUGGEST</button>
          </div>
        </div>
      </div>
    )
  }
}
