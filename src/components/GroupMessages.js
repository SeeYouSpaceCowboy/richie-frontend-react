import React, { Component } from 'react'
import ActionCable from 'actioncable'
import { findDOMNode } from 'react-dom'
import $ from 'jquery'
import { fbHeaderManager } from '../managers/fbHeaderManager'

class GroupMessages extends Component {
  constructor(props) {
    super(props)
    this.state = {
      messageInput: '',
      messages: props.currentGroup.messages
    }

    this.establishConnection  = this.establishConnection.bind(this)
    this.handleMessageInput   = this.handleMessageInput.bind(this)
    this.handleMessageEnter   = this.handleMessageEnter.bind(this)
    this.handleMessagePost    = this.handleMessagePost.bind(this)
    this.updateGroupMessages  = this.updateGroupMessages.bind(this)
  }

  scrollToBottom() {
    const el = findDOMNode(this.refs.messages)
    $(el).scrollTop($(el).prop("scrollHeight"))
  }

  handleMessageInput(event) {
    let messageInput = this.state.messageInput
    messageInput = event.target.value
    return this.setState({ messageInput })
  }

  handleMessageEnter(event) { if(event.key === 'Enter') this.handleMessagePost() }

  handleMessagePost() {
    this.App.chat.speak(this.state.messageInput, this.props.currentUser.id, this.props.currentGroup.id)
    this.setState({ messageInput: '' })
    document.getElementById('group-message-input').value = ''
  }

  updateGroupMessages(data) {
    let messages = this.state.messages

    messages.push({ message: { content: data.message.content, user_id: data.user.id }, user_image: data.user.image, user_name: data.user.name })
    this.setState({ messages })
  }

  establishConnection() {
    if(!this.App) this.App = {}
    this.App.cable = ActionCable.createConsumer(`wss://richie-isuruv.c9users.io/group_messages/?access-token=${ fbHeaderManager.getHeaders().headers['ACCESS_TOKEN']}&client=${ fbHeaderManager.getHeaders().headers['CLIENT'] }&uid=${ fbHeaderManager.getHeaders().headers['UID'] }`)
    this.App.chat = this.App.cable.subscriptions.create(
      { channel: "GroupChatChannel", group_id: this.props.currentGroup.id },
      {
        received(data) {
          this.updateGroupMessages(data)
        },
        speak(msg, user_id, group_id) {
          return this.send({ action: 'send_message', content: msg, group_id: group_id, user_id: user_id })
        },
        updateGroupMessages: this.updateGroupMessages
      }
    )
  }

  componentWillMount() { this.establishConnection() }

  componentDidMount() { this.scrollToBottom() }

  shouldComponentUpdate(nextProps, nextState) {
    this.state.messageInput !== nextState.messageInput)
  }

  componentWillUpdate(nextProps, nextState) {
    if(nextProps.currentGroup !== this.props.currentGroup) {
      this.setState({ messages:  nextProps.currentGroup.messages })
    }

    this.App.cable.disconnect()
    this.establishConnection()
  }

  componentDidUpdate() { this.scrollToBottom() }

  componentWillUnmount() { this.App.cable.disconnect() }

  render() {
    let groupMessages = this.state.messages
    let currentUser = this.props.currentUser

    return (
      <div className='group-messaging'>
        <h3 className='heading-1'>Messages</h3>
        <div className='messages' ref='messages'>
          {
            groupMessages.map((message, i) => {
              if(message.message.user_id === currentUser.id) {
                return (
                  <div key={ i } className='row'>
                    <div className='col-9 right-message-bubble card'>{ message.message.content }</div>
                    <div className='col-3'>
                      <img className='circle icon' alt='profile' src={ currentUser.image }/>
                    </div>
                  </div>
                )
              } else {
                return (
                  <div key={ i } className='row'>
                    <div className='col-3'>
                      <img className='circle icon' alt='profile' src={ message.user_image }/>
                    </div>
                    <div className='col-9 left-message-bubble card'>{ message.message.content }</div>
                  </div>
                )
              }
            })
          }
        </div>

        <div className='row group-message-input'>
          <input id='group-message-input' className='write-message col-10' placeholder='Type a message' onChange={ this.handleMessageInput } onKeyDown={ this.handleMessageEnter }/>
          <button className='col-2' onClick={ this.handleMessagePost }>SEND</button>
        </div>
      </div>
    )
  }
}

export default (GroupMessages)
