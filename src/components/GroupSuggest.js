import React, { Component } from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import ActionCable from 'actioncable'
import { fbHeaderManager } from '../managers/fbHeaderManager'
import{ fetchAllETFs } from '../actions/etfActions'
import{ suggestEtf } from '../actions/groupActions'

class GroupSuggest extends Component {
  constructor() {
    super()
    this.handleSuggestSubmit = this.handleSuggestSubmit.bind(this)
  }

  establishConnection() {
    if(!this.App) this.App = {}
    this.App.cable = ActionCable.createConsumer(`wss://richie-isuruv.c9users.io/group_messages/?access-token=${ fbHeaderManager.getHeaders().headers['ACCESS_TOKEN']}&client=${ fbHeaderManager.getHeaders().headers['CLIENT'] }&uid=${ fbHeaderManager.getHeaders().headers['UID'] }`)
    this.App.suggest = this.App.cable.subscriptions.create(
      { channel: "SuggestionChannel", group_id: this.props.params.group_id },
      {
        speak(group_id, etf_id, etf_symbol, amount) {
          return this.send({ action: 'send_suggestion', group_id, etf_id, etf_symbol, amount })
        }
      }
    )
  }

  handleSuggestSubmit() {
    let groupId = parseInt(this.props.params.group_id, 10)
    let etfId   = parseInt(document.getElementById('selected-etf').name, 10)
    let ticker  = document.getElementById('selected-etf').value
    let amount  = parseInt(document.getElementById('amount').value, 10)

    this.App.suggest.speak(groupId, etfId, ticker, amount)

    browserHistory.push('/groups')
  }

  handleETFClick(i, e) {
    let etf = this.props.etfState.etfs[i]
    let $selected_etf = document.getElementById('selected-etf')
    $selected_etf.value = etf.ticker
    $selected_etf.name = etf.id
  }

  componentWillMount() {
    this.props.fetchAllETFs()
    this.establishConnection()
  }

  render() {
    let etfs = this.props.etfState.etfs
    if(Object.keys(etfs).length === 0 && etfs.constructor === Object) return null

    return(
      <div>
        <h1>Suggest</h1>
          {
            etfs.map((etf, i) => {
              return (
                <div key={ i } className='card row' onClick={ this.handleETFClick.bind(this, i) }>
                  <div className='col-12'>
                    <p>{ etf.name } { etf.ticker }</p>
                  </div>
                </div>
              )
            })
          }

        <input id='selected-etf' className='fill' value='Select an ETF' name='What' readOnly/>

        <div className='row'>
          <div className='col-12'>
            <label>Dollar Amount</label>
            <br/>
            <input id='amount' className='fill' placeholder='$'/>
          </div>
        </div>
        <button onClick={ this.handleSuggestSubmit }>Submit</button>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    etfState: state.etf
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchAllETFs: () => {
      let actions = fetchAllETFs()
      dispatch(actions)
    },
    suggestEtf: suggestion => {
      let actions = suggestEtf(suggestion)
      dispatch(actions)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupSuggest)
