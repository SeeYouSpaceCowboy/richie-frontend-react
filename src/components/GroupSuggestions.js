import React, { Component } from 'react'
import ActionCable from 'actioncable'
import { fbHeaderManager } from '../managers/fbHeaderManager'

class GroupSuggestions extends Component {
  constructor(props) {
    super(props)
    this.state = {
      groupId: props.group.id,
      suggestions: props.suggestions
    }

   this.updateSuggestions = this.updateSuggestions.bind(this)
  }

  establishConnection() {
    if(!this.App) this.App = {}
    this.App.cable = ActionCable.createConsumer(`wss://richie-isuruv.c9users.io/group_messages/?access-token=${ fbHeaderManager.getHeaders().headers['ACCESS_TOKEN']}&client=${ fbHeaderManager.getHeaders().headers['CLIENT'] }&uid=${ fbHeaderManager.getHeaders().headers['UID'] }`)
    this.App.suggest = this.App.cable.subscriptions.create(
      { channel: "SuggestionChannel", group_id: this.state.groupId },
      {
        received(data) {
          this.updateSuggestions(data)
        },
        speak(group_id, etf_id, etf_symbol, amount) {
          return this.send({ action: 'send_suggestion', group_id, etf_id, etf_symbol, amount })
        },
        updateSuggestions: this.updateSuggestions
      }
    )
  }

  updateSuggestions(data) {
    const { investment_request } = data
    let suggestions = this.state.suggestions
    suggestions.push(investment_request)

    this.setState({suggestions})
  }

  componentWillMount() { this.establishConnection() }

  componentWillUpdate(nextProps, nextState) {
    if(this.props.group.id !== nextProps.group.id) {
      this.setState({ groupId: nextProps.group.id })
    }

    this.App.cable.disconnect()
    this.establishConnection()
  }

  componentWillUnmount() { this.App.cable.disconnect() }

  render() {
    let suggestions = this.props.suggestions

    if(suggestions.length === 0) {
      return (
        <div className='card'>
          <p>^ Make a suggestion!</p>
        </div>
      )
    }

    return (
      <div className='suggestions-container'>
        {
          suggestions.map((suggestion, i) => {
            return (
              <div key={ i } className='card-border row'>
                <div className='row'>
                  <div className='col-4'>
                    <img className='circle icon' alt='user' src={ suggestion.requester.image }/>
                  </div>
                  <div className='col-8'>
                    <h4>{ suggestion.requester.name }</h4>
                  </div>
                </div>
                <div className='row'>
                  <div className='small-space'/>
                  <p>$ { suggestion.amount }</p>
                  <p>{ suggestion.etf.name }</p>
                </div>
                <div className='row'>
                  <div className='col-6'>
                    <button className='fill'>ACCEPT</button>
                  </div>
                  <div className='col-6'>
                    <button className='fill'>DECLINE</button>
                  </div>
                </div>
              </div>
            )
          })
        }
      </div>
    )
  }
}

export default (GroupSuggestions)
