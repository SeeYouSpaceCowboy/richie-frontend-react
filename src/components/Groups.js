import React, { Component } from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { fetchCurrentUser } from '../actions/userActions'
import { fetchGroup } from '../actions/groupActions'
import GroupMessages from './GroupMessages'
import GroupSuggestions from './GroupSuggestions'

class Groups extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentGroupIndex: 0,
      group: props.groupState.group
    }

    this.handleCreateGroup = this.handleCreateGroup.bind(this)
  }

  renderGroups(currentUser) {
    if(currentUser.groups.length === 0) return null

    return (
      <div>
        {
          currentUser.groups.map((group, i) => {
            return (
              <div className='group' key={ i } onClick={ this.handleGroupClick.bind(this, i) }>
                <h4>{ group.name.charAt(0).toUpperCase() + group.name.slice(1) }</h4>
              </div>
            )
          })
        }
      </div>
    )
  }

  renderGroupSuggestions(currentGroup) {
    return (
      <div>
        <div className='row'>
          <h3 className='heading-1'>Suggestions</h3>
        </div>
        <GroupSuggestions suggestions={ currentGroup.group_investment_requests } group={ currentGroup }/>
      </div>
    )
  }

  handleGroupClick(i, e) {
    this.setState({ currentGroupIndex: i })
    this.props.fetchGroup( this.props.userState.currentUser.groups[i].id )
  }

  handleGroupSuggest(groupId) {
    browserHistory.push(`/group-suggest/${ groupId }`)
  }

  handlePostClick() {
    let state = this.state
    this.App.chat.speak(state.message, state.currentUserID, state.currentGroupIndex)
  }

  handleCreateGroup() { browserHistory.push('/create-group') }

  render() {
    let currentUser = this.props.userState.currentUser
    if(Object.keys(currentUser).length === 0 && currentUser.constructor === Object) return null

    if(currentUser.groups.length === 0) {
      return (
        <div>
          <div className='row'>
            <div className='col-12'>
              <button onClick={ this.handleCreateGroup }>Create Group</button>
            </div>
          </div>

          <div className='card row'>
            <div className='col-12'>
              <p>You dont have any groups, create a group to get started!</p>
            </div>
          </div>
        </div>
      )
    }


    let currentGroup = this.props.groupState.group
    if(Object.keys(currentGroup).length === 0 && currentGroup.constructor === Object){
      currentGroup = currentUser.groups[this.state.currentGroupIndex]
    }

    return (
      <div>
        <div className='row'>
          <div className='col-12'>
            <button onClick={ this.handleCreateGroup }>CREATE GROUP</button>
          </div>
        </div>

        <div className='row group-messaging-container'>
          <div className='col-2'>{ this.renderGroups(currentUser) }</div>

          <div className='col-6'>
            <GroupMessages currentGroup={ currentGroup } currentUser={ currentUser }/>
          </div>

          <div className='col-4 group-dashboard fill-height'>
            <div className='row'>
              <div className='row'>
                <div className='col-8'>
                  <h1 className='heading-1'>{ currentGroup.name.charAt(0).toUpperCase() + currentGroup.name.slice(1)}</h1>
                </div>
                <div className='col-4'>
                  <button className='fill' onClick={ this.handleGroupSuggest.bind(this, currentGroup.id) }>SUGGEST</button>
                </div>
              </div>
              <div className='row'>
                <p>{ currentGroup.description.charAt(0).toUpperCase() + currentGroup.description.slice(1) }</p>
              </div>
              <div className='small-space' />
            </div>

            <div className='row'>
              <h3 className='heading-1'>Members</h3>
              { currentGroup.users.map((member, i) => <p key={ i }>{ member.name }</p>) }
              <div className='small-space' />
            </div>

            <div className='row'>{ this.renderGroupSuggestions(currentGroup) }</div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    userState: state.user,
    groupState: state.groups
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchCurrentUser: () => {
      let action = fetchCurrentUser()
      dispatch(action)
    },
    fetchGroup: id => {
      let action = fetchGroup(id)
      dispatch(action)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Groups)
