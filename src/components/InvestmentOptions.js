import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchAllETFs } from '../actions/etfActions'

class InvestmentOptions extends Component {
  componentWillMount() {
    this.props.fetchAllETFs()
  }

  onEtfHover(etf, e) {
    console.log('On Focus')
    debugger
  }

  render() {
    let etfs = this.props.etfState.etfs
    if(Object.keys(etfs).length === 0 && etfs.constructor === Object) return null

    let dummyEtfImgs = [
      'https://static.pexels.com/photos/160211/pexels-photo-160211.jpeg',
      'https://static.pexels.com/photos/245618/pexels-photo-245618.jpeg',
      'https://static.pexels.com/photos/157039/pexels-photo-157039.jpeg',
      'https://static.pexels.com/photos/5980/food-sunset-love-field.jpg',
      'https://static.pexels.com/photos/164560/pexels-photo-164560.jpeg',
      'https://static.pexels.com/photos/355956/pexels-photo-355956.jpeg',
      'https://static.pexels.com/photos/24859/pexels-photo-24859.jpg',
      'https://static.pexels.com/photos/213663/pexels-photo-213663.jpeg',
      'https://static.pexels.com/photos/363193/pexels-photo-363193.jpeg',
      'https://static.pexels.com/photos/324629/pexels-photo-324629.jpeg',
      'https://static.pexels.com/photos/38924/pexels-photo-38924.jpeg'
    ]

    return (
      <div>
        <div className='row'>
          {
            etfs.map((etf, i) => {
              let randomIndex = Math.floor(Math.random() * dummyEtfImgs.length)

              return (
                <div key={ i } className='col-4'>
                  <div className='index-fund-container'>
                    <div className='index-fund' onClick={ this.onEtfHover.bind(this, etf) } onFocus={ this.onEtfHover.bind(this, etf)}>
                      <img className='index-img' alt='index-fund' src={ dummyEtfImgs[randomIndex] } width='100%' height='100%'/>
                    </div>

                    <div className='overlay'>
                      <p>{ etf.fiduty_name }</p>
                      <p>{ etf.ticker }</p>
                    </div>
                  </div>
                </div>
              )
            })
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    etfState: state.etf
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchAllETFs: () => {
      let action = fetchAllETFs()
      dispatch(action)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InvestmentOptions)
