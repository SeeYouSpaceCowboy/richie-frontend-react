import React, { Component } from 'react'
import { connect } from 'react-redux'
import { postUserFacebookResponse } from '../actions/userActions'
import FacebookLogin from 'react-facebook-login'

class Login extends Component {
  constructor() {
    super()

    this.responseFacebook = this.responseFacebook.bind(this)
  }

  responseFacebook(response) {
    const { name, email, userID, accessToken } = response
    const { min } = response.age_range
    const { url } = response.picture.data

    const facebook_user = {
      user: {
        fd_id: userID,
        access_token: accessToken,
        name,
        email,
        age: min,
        image: url
      }
    }

    this.props.postUserFacebookResponse(facebook_user)
  }

  render(){
    return(
      <div>
        <FacebookLogin
          appId     = "1418855321467898"
          autoLoad  = { false }
          fields    = "name, email, picture, age_range, birthday, education"
          callback  = { this.responseFacebook } />
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    postUserFacebookResponse: (facebook_user) => {
      let action = postUserFacebookResponse(facebook_user)
      dispatch(action)
    }
  }
}

export default connect(null, mapDispatchToProps)(Login)
