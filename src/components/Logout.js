import React, { Component } from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { logout } from '../actions/userActions'
import { fbHeaderManager } from '../managers/fbHeaderManager'

class Logout extends Component {
  constructor() {
    super()

    this.handleLogOut = this.handleLogOut.bind(this)
  }

  handleLogOut() {
    this.props.logout()
    fbHeaderManager.removeHeaders()
    browserHistory.push('/login')
  }

  render() {
    return (
      <div onClick={ this.handleLogOut } >
        <p>Log Out</p>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logout: () => {
      let action = logout()
      dispatch(action)
    }
  }
}

export default connect(null, mapDispatchToProps)(Logout)
