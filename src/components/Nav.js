import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { fetchCurrentUser } from '../actions/userActions'
import Search from './Search.js'
import Logout from './Logout.js'

class Nav extends Component {
  constructor() {
    super()

    this.handleProfileClick = this.handleProfileClick.bind(this)
    this.handleDropdownClick = this.handleDropdownClick.bind(this)
  }

  handleProfileClick() {
    document.getElementById("profile-dropdown").style.display = 'block'
  }

  handleDropdownClick() {
    document.getElementById("profile-dropdown").style.display = 'none'
  }

  componentWillMount() {
    this.props.fetchCurrentUser()
  }

  render() {
    let currentUser = this.props.userState.currentUser
    if(!currentUser || ((Object.keys(currentUser).length === 0 && currentUser.constructor === Object))) return null

    if(!sessionStorage['access-token']){
      return (
        <div>
          <div className='hidden_nav'/>
          <div className='nav'>
            <div className='nav-item'>
              <img className='icon' alt='home' src='./images/fiduty-logo.png' />
            </div>
          </div>
        </div>
      )
    }

    return (
      <div>
        <div className='hidden_nav'/>
        <div className='nav'>
          <div className='nav-item'>
            <Link to='/home' action='replace'><img className='icon' alt='home' src='./images/fiduty-logo.png' /></Link>
          </div>
          <div className='nav-item'>
            <Search />
          </div>
          <div className='right nav-item dropdown'>
            <img className='circle icon' alt='profile' src={ currentUser.image } onClick={ this.handleProfileClick }/>
            <div id="profile-dropdown" className="dropdown-content" onBlur={ this.handleDropdownClick }>
              <Link to='profile' action='replace' onClick={ this.handleDropdownClick }><p>Profile</p></Link>
              <Link to='notifications' action='replace' onClick={ this.handleDropdownClick }><p>Notifications</p></Link>
              <Link to='' onClick={ this.handleDropdownClick }><p>Settings</p></Link>
              <a onClick={ this.handleDropdownClick }><Logout /></a>
            </div>
          </div>
          <div className='right nav-item'>
            <Link to='groups' action='replace'><img className='icon delete-me-shrink-icon' alt='groups' src='./images/groups.png' /></Link>
          </div>
          <div className='right nav-item'>
            <Link to='investments' action='replace'><img className='icon delete-me-shrink-icon' alt='investments' src='./images/stocks.png' /></Link>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    userState: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchCurrentUser: () => {
      let action = fetchCurrentUser()
      dispatch(action)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Nav)
