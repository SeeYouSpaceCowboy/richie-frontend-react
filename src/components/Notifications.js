import React, { Component } from 'react'
import { connect } from 'react-redux'
import { groupRequestResponse } from '../actions/groupActions'
import { fetchCurrentUser } from '../actions/userActions'

class Notifications extends Component {
  constructor(props) {
    super(props)
    this.state = {
      minimum: this.props.userState.currentUser || 0
    }
  }

  handleRequest(id, choice) {
    this.props.groupRequestResponse(id, choice)
  }

  componentWillMount() {
    this.props.fetchCurrentUser()
  }

  render() {
    let currentUser = this.props.userState.currentUser
    if(Object.keys(currentUser).length === 0 && currentUser.constructor === Object) return null
    debugger

    if(currentUser.group_requests.length === 0) {
      return (
        <div className='card'>
          <p>You have no notifications at this moment :)</p>
        </div>
      )
    }

    return (
      <div>
        {
          currentUser.group_requests.map((request, i) => {
            return (
              <div key={ i } className='card row'>
                <div className='row'>
                  <div className='col-12'>
                    { request.message } with a minimum of $ { request.minimum_amount }
                  </div>
                </div>
                <div className='row'>
                  <div className='col-8'>
                    <input className='fill' placeHolder='minimum' />
                  </div>
                  <div className='col-2'>
                    <button className='fill' onClick={ this.handleRequest.bind(this, request.id, true) }>Approve</button>
                  </div>
                  <div className='col-2'>
                    <button className='fill' onClick={ this.handleRequest.bind(this, request.id, false) }>Ignore</button>
                  </div>
                </div>
              </div>
            )
          })
        }
      </div>
    )
  }
}

const connectStateToProps = state => {
  return {
    userState: state.user
  }
}

const connectDispatchToProps = dispatch => {
  return {
    fetchCurrentUser: () => {
      let action = fetchCurrentUser()
      dispatch(action)
    },
    groupRequestResponse: (id, choice) => {
      let action = groupRequestResponse(id, choice)
      dispatch(action)
    }
  }
}

export default connect(connectStateToProps, connectDispatchToProps)(Notifications)
