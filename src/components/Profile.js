import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchCurrentUser } from '../actions/userActions'

class Profile extends Component {
  componentWillMount() {
    this.props.fetchCurrentUser()
  }

  componentDidMount() { }

  render() {
    let currentUser = this.props.userState.currentUser
    if(Object.keys(currentUser).length === 0 && currentUser.constructor === Object) return null

    let groups = currentUser.groups
    return (
      <div>
        <div className='row'>
          <div className='col-4'>
            <img className='center circle' width='100px' alt='profile' src={ currentUser.image }/>
            <h2>{ currentUser.name }</h2>
            <p>
              Followers { currentUser.followers.length } &nbsp;&nbsp;&nbsp;&nbsp;
              Followings { currentUser.followers.length }
            </p>
          </div>
          <div className='col-8'>
            <h1 className='mono'>Balance $ { currentUser.amount }</h1>
          </div>
        </div>

        <div className='row'>
          <div className='col-12'>
            <h2 className='heading-1'>Groups</h2>
          </div>
        </div>

        {
          groups.map((group, i) => {
            return (
              <div className='card row' key={ i }>
                <div className='col-9'>
                  <p>{ group.name }</p>
                </div>
                <div className='col-3'>
                  <p className='mono'>$0</p>
                </div>
              </div>
            )
          })
        }

        <div className='row'>
          <div className='col-12'>
            <h2 className='heading-1'>Individual</h2>
          </div>
        </div>

        <div>
          <div className='card row'>
            <div className='col-9'>
              <p>Mature Stocks</p>
            </div>
            <div className='col-3'>
              <p className='mono'>$200 +$20</p>
            </div>
          </div>
          <div className='card row'>
            <div className='col-9'>
              <p>Technology</p>
            </div>
            <div className='col-3'>
              <p className='mono'>$132 -$8</p>
            </div>
          </div>
          <div className='card row'>
            <div className='col-9'>
              <p>International Trade</p>
            </div>
            <div className='col-3'>
              <p className='mono'>$346 -$34</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    userState: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchCurrentUser: () => {
      let action = fetchCurrentUser()
      dispatch(action)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
