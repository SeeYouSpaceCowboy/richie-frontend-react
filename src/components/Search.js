import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchSearchResults } from '../actions/searchActions'
import UserSearchResults from './UserSearchResults'

class Search extends Component {
  constructor() {
    super()

    this.handleSearchInputChange = this.handleSearchInputChange.bind(this)
  }

  handleSearchInputChange() {
    let input = this.refs.search.value.toLowerCase()
    this.props.fetchSearchResults(input)
  }

  renderResults() {
    let results = this.props.searchState.search
    if(Object.keys(results).length === 0 && results.constructor === Object) return null

    return (
      <div className='nav-search-results'>
        {
          results.map((user, i) => {
            return <UserSearchResults key={ i } userResults={ user } />
          })
        }
      </div>
    )
  }

  render() {
    return (
      <div>
        <input
          ref='search'
          className='nav-search'
          placeholder='search'
          onChange={ this.handleSearchInputChange } />

          {
            this.renderResults()
          }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    searchState: state.search
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchSearchResults: (input) => {
      let action = fetchSearchResults(input)
      dispatch(action)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)
