import React, { Component } from 'react'
import { browserHistory } from 'react-router'

class UserSearchResults extends Component {
  constructor() {
    super()
    this.state = {
      user: {}
    }

    this.handleUserResultsClick = this.handleUserResultsClick.bind(this)
  }

  componentWillMount() {
    this.setState({
      user: this.props.userResults
    })
  }

  render() {
    let userResults = this.state.user
    return (
      <div className='nav-search-results-items' onClick={ this.handleUserResultsClick }>
        <img className='icon' alt='user profile' src={ userResults.image }/>
        <p>{ userResults.name }</p>
      </div>
    )
  }

  handleUserResultsClick() {
    // debugger
    // browserHistory.push(`/home`)
    browserHistory.push(`/profile/${ this.state.user.id }`)
  }
}

export default (UserSearchResults)
