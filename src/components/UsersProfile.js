import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchUser } from '../actions/userActions'

class UsersProfile extends Component {
  constructor() {
    super()

    this.renderFollowButton = this.renderFollowButton.bind(this)
  }

  componentWillMount() {
    // debugger
    this.props.fetchUser(this.props.params.uid)
  }

  render() {
    let user = this.props.userState.user

    return (
      <div>
        <img className='icon' alt='user profile' src={ user.image } />
        <p>{ user.name }</p>
        { this.renderFollowButton() }
      </div>
    )
  }

  renderFollowButton() {
    return <button>Follow</button>
  }
}

const mapStateToProps = state => {
  return {
    userState: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchUser: (userID) => {
      let action = fetchUser(userID)
      dispatch(action)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersProfile)
