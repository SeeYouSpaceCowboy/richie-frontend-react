const CONTENT_TYPE  = 'Content-Type'
const ACCESS_TOKEN  = 'access-token'
const CLIENT        = 'client'
const EXPIRY        = 'expiry'
const TOKEN_TYPE    = 'token-type'
const UID           = 'uid'

export const fbHeaderManager = {
  getHeaders: () => {
    return {
      headers: {
        CONTENT_TYPE: sessionStorage.getItem(CONTENT_TYPE),
        ACCESS_TOKEN: sessionStorage.getItem(ACCESS_TOKEN),
        CLIENT:       sessionStorage.getItem(CLIENT),
        EXPIRY:       sessionStorage.getItem(EXPIRY),
        TOKEN_TYPE:   sessionStorage.getItem(TOKEN_TYPE),
        UID:          sessionStorage.getItem(UID),
      }
    }
  },

  setHeaders: headers => {
    sessionStorage.setItem(ACCESS_TOKEN, headers[ACCESS_TOKEN])
    sessionStorage.setItem(CLIENT, headers[CLIENT])
    sessionStorage.setItem(CONTENT_TYPE, 'application/json')
    sessionStorage.setItem(EXPIRY, headers[EXPIRY])
    sessionStorage.setItem(TOKEN_TYPE, headers[TOKEN_TYPE])
    sessionStorage.setItem(UID, headers[UID])
  },

  removeHeaders: () => {
    sessionStorage.removeItem(ACCESS_TOKEN)
    sessionStorage.removeItem(TOKEN_TYPE)
    sessionStorage.removeItem(CLIENT)
    sessionStorage.removeItem(EXPIRY)
    sessionStorage.removeItem(UID)
    sessionStorage.removeItem(CONTENT_TYPE)
  }
}
