export default function etfReducer(state = { etfs: {} }, action) {
  switch(action.type) {
    case 'FETCH_ALL_ETFS': return { ...state, etfs: action.payload }
    default: return state
  }
}
