export default function groupReducer(state = { group: {}, createGroup: {} }, action) {
  switch(action.type) {
    case 'FETCH_GROUP': return { ...state, group: action.payload }
    case 'CREATE_GROUP': return { ...state, createGroup: action.payload }
    default: return state
  }
}
