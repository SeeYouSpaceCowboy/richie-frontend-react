import { combineReducers } from 'redux'
import userReducer from './userReducer'
import etfReducer from './etfReducer'
import searchReducer from './searchReducer'
import groupReducer from './groupReducer'

const rootReducer = combineReducers({
  user:   userReducer,
  etf:    etfReducer,
  groups: groupReducer,
  search: searchReducer
})

export default rootReducer
