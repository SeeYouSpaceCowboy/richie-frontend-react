import { browserHistory } from 'react-router'

export default function userReducer(state = { currentUser : {}, user: {} }, action) {
  switch(action.type) {
    case 'POST_USER_FACEBOOK_RESPONSE':
      browserHistory.push('/')
      return action.payload
    case 'FETCH_CURRENT_USER': return { ...state, currentUser: action.payload }
    case 'FETCH_USER': return { ...state, user: action.payload }
    default: return state
  }
}
