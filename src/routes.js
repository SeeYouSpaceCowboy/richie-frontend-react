import React from 'react'
import { IndexRoute, Route } from 'react-router'
import App from './App'
import Login from './components/Login'
import Feed from './components/Feed'
import Profile from './components/Profile'
import UsersProfile from './components/UsersProfile'
import Notifications from './components/Notifications'
import InvestmentOptions from './components/InvestmentOptions'
import Groups from './components/Groups'
import GroupSuggest from './components/GroupSuggest'
import CreateGroup from './components/CreateGroup'

export default (
  <Route path='/' component={ App }>
    <IndexRoute component={ Feed } onEnter={ requireAuth }/>
    <Route path='/login' component={ Login } />
    <Route path='/home' component={ Feed } onEnter={ requireAuth }/>
    <Route path='/profile' component={ Profile } onEnter={ requireAuth }/>
    <Route path='/notifications' component={ Notifications } onEnter={ requireAuth }/>
    <Route path='/profile/:uid' component={ UsersProfile } onEnter={ requireAuth }/>
    <Route path='/investments' component={ InvestmentOptions } onEnter={ requireAuth }/>
    <Route path='/groups' component={ Groups } onEnter={ requireAuth }/>
    <Route path='/create-group' component={ CreateGroup } onEnter={ requireAuth }/>
    <Route path='/group-suggest/:group_id' component={ GroupSuggest } onEnter={ requireAuth }/>
  </Route>
)

function requireAuth(nextState, replace) {
  if (!sessionStorage['access-token']) replace({ pathname: '/login' })
}
